#!/usr/bin/env python
# coding: utf-8

# # Rapport de suivi d'avancement:

# **Exécution du bloc ci-dessous obligatoire**

# In[1]:


import findspark
findspark.init()
from pyspark.sql import SparkSession
import pyspark
import random
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pandas.plotting import scatter_matrix
from pyspark.sql import functions as F
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.seasonal import seasonal_decompose
from statsmodels.tsa.arima_model import ARIMA 
import statsmodels.api as sm
#from pandas.plotting import register 
#register_matplotlibmaters_conplers_conplers_conplers_conplats_conplers_conplers_conplats_conplats_convers
get_ipython().run_line_magic('matplotlib', 'nbagg')

from pyspark.sql import functions as func
from pyspark.sql.window import Window


get_ipython().run_line_magic('matplotlib', 'notebook')

sc = pyspark.SparkContext(appName="Pi")
spark = SparkSession.builder.appName("SimpleApp").getOrCreate()


data = spark.read.csv("data.csv", header=True, sep=",")


# ## I- Débruitage:

# ### 1- Ecart Interquartile (Box Plot)

# In[4]:


data1 = data


# In[5]:


x1 = data1.toPandas()["NO2"].values.tolist()
x2 = data1.toPandas()["PM10"].values.tolist()
x3 = data1.toPandas()["PM2.5"].values.tolist()
x4 = data1.toPandas()["PM1.0"].values.tolist()
x5 = data1.toPandas()["BC"].values.tolist()


# In[6]:


def toNaN(x):
    for i in range(0,len(x)):
        if (i == 0):
            x[i] = 0
        if (x[i] == "NULL"):
            x[i] = np.nan
    return x

def toSTR(x):
    for i in range(0,len(x)):
        x[i] = round(x[i],1)
    return x


def toInt(x):
    for i in range(0,len(x)):
        if (x[i] == "NULL"):
            x[i] = 0
        else:
            x[i] = int(x[i])
    return x

def noiseDetector(L,limSup):
    res = []
    for i in L:
        if i > limSup:
            res.append(i)
            
    return res


# #### a) NO2:

# In[7]:


#z = toInt(x)
print(len(x1))
z = map(lambda a:a,filter(lambda a:a != 'NULL', x1))
z = toInt(list(z))
print(len(z))
#print(z)
#plt.size(200)
plt.figure(figsize=(10,10))
plt.boxplot(list(z), widths = 0.6, patch_artist = True)


# #### b) PM10:

# In[8]:


print(len(x2))
z1 = map(lambda a:a,filter(lambda a:a != 'NULL', x2))
z1 = toInt(list(z1))
print(len(z1))
#plt.size(200)
plt.figure(figsize=(10,10))
plt.boxplot(z1, widths = 0.6, patch_artist = True)


# #### c) PM2.5

# In[9]:


print(len(x3))
z2 = map(lambda a:a,filter(lambda a:a != 'NULL', x3))
z2 = toInt(list(z2))
print(len(z2))
#plt.size(200)
plt.figure(figsize=(10,10))
plt.boxplot(z2, widths = 0.6, patch_artist = True)


# #### d) PM1.0

# In[10]:


print(len(x4))
z3 = map(lambda a:a,filter(lambda a:a != 'NULL', x4))
z3 = toInt(list(z3))
print(len(z3))
#plt.size(200)
plt.figure(figsize=(10,10))
plt.boxplot(z3, widths = 0.6, patch_artist = True)


# #### e) BC

# In[11]:


print(len(x5))
z4 = map(lambda a:a,filter(lambda a:a != 'NULL', x5))
z4 = toInt(list(z4))
print(len(z4))
#plt.size(200)
plt.figure(figsize=(10,10))
plt.boxplot(z4, widths = 0.6, patch_artist = True)


# #### f) Résumé des indicateurs dans l'ordre

# In[12]:


fig = plt.figure(figsize=(20,10))
plt.subplot(2, 3, 1)
plt.boxplot(z, widths = 0.6, patch_artist = True)
plt.subplot(2, 3, 2)
plt.boxplot(z1, widths = 0.6, patch_artist = True)
plt.subplot(2, 3, 3)
plt.boxplot(z2, widths = 0.6, patch_artist = True)
plt.subplot(2, 3, 4)
plt.boxplot(z3, widths = 0.6, patch_artist = True)
plt.subplot(2, 3, 5)
plt.boxplot(z4, widths = 0.6, patch_artist = True)


# In[34]:


plt.close()


# *Conclusions:* On voit bien que les colonnes commençant par PM... ont à peu près la même répartition des données sauf la dernière colonne BC. 

# ### 2) Moving Average

# In[2]:


""" Exécution du fichier utils comportant certaines fonctions que nous allons utiliser """
get_ipython().run_line_magic('run', 'utils.ipynb')


# In[3]:


"""Chargement du jeu de données, qui doit etre contenu dans le workspace"""
"""Puis suppression des doublons et renomage de certaines colonnes"""
df_2 = spark.read.csv('VGP-week3-data.csv', inferSchema=True, header=True, timestampFormat='yyyy-MM-dd HH:mm:ss+ss')
df_2 = df_2.dropDuplicates()
df_2 = df_2.orderBy("time")
df_2 = df_2.withColumnRenamed("PM2.5", "PM2")
df_2 = df_2.withColumnRenamed("PM1.0", "PM1")


# In[4]:


#function to calculate number of seconds from number of days: thanks Bob Swain
days = lambda i: i * 86400


# In[4]:


"""Create a five day Window from five days previous to the current day (row), using previous casting of timestamp to long (number of seconds). """ 
"""remember a start value of "-1" means one off before the current row, and we are taking the timestamp as a long and comparing it to the rangeBetween amount of time.""" 
"""remember an end value of 0 is the current row."""

windowSpec = Window.orderBy(func.col("time").cast('long')).rangeBetween(-300, 0)


# In[5]:


"""Write a custom function to convert the data type of DataFrame columns""" 
def convertColumn(df, names, newType):
  for name in names: 
     df = df.withColumn(name, df[name].cast(newType))
  return df 


#     a) NO2 Moving Average

# In[6]:


df2_NO2 = df_2.select("time","NO2")

df2_NO2 = df2_NO2.filter(df2_NO2.NO2 != 'NULL')

# Assign column names to `columns`
columns = ['NO2']

# Conver the `NO2` columns to `IntegerType()`
df2_NO2 = convertColumn(df2_NO2, columns, IntegerType())


# In[13]:


# Note the OVER clause added to AVG(), to define a windowing column.
df2_NO2 = df2_NO2.withColumn('rolling_five_minute_average', func.avg("NO2").over(windowSpec))
df2_NO2.show(30)


# In[14]:


df2_NO2_ma = df2_NO2.toPandas()
df2_NO2_ma = df2_NO2_ma.set_index('time')
df2_NO2_ma.plot(figsize=(10, 5), title='NO2 values')


#        b) PM10 Moving Average

# In[15]:


df2_PM10 = df_2.select("time","PM10")

df2_PM10 = df2_PM10.filter(df2_PM10.PM10 != 'NULL')

# Assign column names to `columns`
columns = ['PM10']

# Conver the `PM10` columns to `IntegerType()`
df2_PM10 = convertColumn(df2_PM10, columns, IntegerType())

# Note the OVER clause added to AVG(), to define a windowing column.
df2_PM10 = df2_PM10.withColumn('moving_five_minute_average', func.avg("PM10").over(windowSpec))

df2_PM10_ma = df2_PM10.toPandas()
df2_PM10_ma = df2_PM10_ma.set_index('time')
df2_PM10_ma.plot(figsize=(10, 5), title='PM10 Moving Average')


#          c) PM2.5 Moving Average

# In[16]:


df2_PM2 = df_2.select("time","PM2")

df2_PM2 = df2_PM2.filter(df2_PM2.PM2 != 'NULL')

# Assign column names to `columns`
columns = ['PM2']

# Conver the `PM2.5` columns to `IntegerType()`
df2_PM2 = convertColumn(df2_PM2, columns, IntegerType())

# Note the OVER clause added to AVG(), to define a windowing column.
df2_PM2 = df2_PM2.withColumn('moving_five_minute_average', func.avg("PM2").over(windowSpec))

df2_PM2_ma = df2_PM2.toPandas()
df2_PM2_ma = df2_PM2_ma.set_index('time')
df2_PM2_ma.plot(figsize=(10, 5), title='PM2.5 Moving Average')


#         d) PM1.0 Moving Average

# In[17]:


df2_PM1 = df_2.select("time","PM1")

df2_PM1 = df2_PM1.filter(df2_PM1.PM1 != 'NULL')

# Assign column names to `columns`
columns = ['PM1']

# Conver the `PM1.0` columns to `IntegerType()`
df2_PM1 = convertColumn(df2_PM1, columns, IntegerType())

# Note the OVER clause added to AVG(), to define a windowing column.
df2_PM1 = df2_PM1.withColumn('moving_five_minute_average', func.avg("PM1").over(windowSpec))

df2_PM1_ma = df2_PM1.toPandas()
df2_PM1_ma = df2_PM1_ma.set_index('time')


#      e) BC Moving Average 

# In[18]:


df2_BC = df_2.select("time","BC")

df2_BC = df2_BC.filter(df2_BC.BC != 'NULL')

# Assign column names to `columns`
columns = ['BC']

# Conver the `BC` columns to `IntegerType()`
df2_BC = convertColumn(df2_BC, columns, IntegerType())

# Note the OVER clause added to AVG(), to define a windowing column.
df2_BC = df2_BC.withColumn('moving_five_minute_average', func.avg("BC").over(windowSpec))

df2_BC_ma = df2_BC.toPandas()
df2_BC_ma = df2_BC_ma.set_index('time')
df2_BC_ma.plot(figsize=(10, 5), title='BC Moving Average')


# ### 3) Moving Median

# In[13]:


df = pd.read_csv("data.csv")

#on définit win_len: c'est la taille de la fenêtre mouvante pour la méthode rolling_median_filter
#On choisit arbitrairement 10
win_len = 10


# In[14]:


df


# #### Rolling median filter sur NO2

# In[31]:


#On travaille uniquement sur une concentration à la fois: dans cet exemple, la concentration de NO2.
df_NO2 = df[['time','NO2']]


# In[35]:


#On plot les valeurs brutes et les valeurs filtrées, c'est à dire la médiane sur une fenêtre mouvante de 10 unités
ax = df_NO2['NO2'].plot(figsize=(10, 5), title='NO2 values')

#Filtre: on utilise les méthodes .rolling() et .median() des dataframes pandas
df_NO2_roll = df_NO2.rolling(win_len).median()
df_NO2_roll.rename(columns={'NO2': 'NO2_median'}, inplace=True)
df_NO2_roll['NO2_median'].plot()


#On affiche le résultat
ax.legend(fontsize=8.5)
plt.tight_layout()
plt.show()


#Pour généraliser, on répète ce processus sur toutes les concentrations.
#Dans le cas de ce filtre, on remplace les valeurs par les médianes sur 10 unités de temps:
#cela permet d'éliminer les valeurs extrêmes mais transforme également les valeurs brutes.
#On pourrait modifier cela en mettant en place une comparaison entre la valeur brute et la valeur médiane.


# In[36]:


#On ferme le plot pour pouvoir en ouvrir un nouveau dans la prochaine cellule
plt.close()


# In[37]:


#On plot les valeurs brutes, les valeurs médianes et les valeurs filtrées.
#On commence par récupérer les valeurs précédentes.
axB = df_NO2['NO2'].plot(figsize=(10, 5), title='NO2 values')

df_NO2_roll['NO2_median'].plot()

#On récupère l'écart type sur la même fenêtre roulante dans df_NO2_std
df_NO2_std = df_NO2.rolling(win_len).std()

#On filtre: les valeurs s'éloignant trop de la médiane ne sont pas gardées
df_NO2_filtered = df_NO2[(df_NO2.NO2 <= df_NO2_roll.NO2_median + 3*df_NO2_std.NO2) & (df_NO2.NO2 >= df_NO2_roll.NO2_median - 3*df_NO2_std.NO2) | pd.isnull(df_NO2.NO2)]
df_NO2_filtered.rename(columns={'NO2': 'NO2_filtered'}, inplace=True)
df_NO2_filtered['NO2_filtered'].plot()

#On récupère les outliers et on les remplace par NaN
df_NO2_outliers = df_NO2[(df_NO2.NO2 > df_NO2_roll.NO2_median + 3*df_NO2_std.NO2) | (df_NO2.NO2 < df_NO2_roll.NO2_median - 3*df_NO2_std.NO2)]
df_NO2_outliers.rename(columns={'NO2': 'NO2_filtered'}, inplace=True)
df_NO2_outliers['NO2_filtered'] = np.nan

#On affiche le résultat
axB.legend(fontsize=8.5)
plt.tight_layout()
plt.show()


# In[38]:


plt.close()


# In[39]:


df_NO2.count()


# In[40]:


df_NO2_final = pd.concat([df_NO2_filtered, df_NO2_outliers])


# In[41]:


df_NO2_final.count()


# In[42]:


df_NO2_final.to_csv(r'NO2_filtered.csv', encoding='utf8', index=False)


# #### Rolling median filter sur PM10

# In[43]:


df_PM10 = df[['time','PM10']]


# In[44]:


#On plot les valeurs brutes et les valeurs filtrées, c'est à dire la médiane sur une fenêtre mouvante de 10 unités
axPM10 = df_PM10['PM10'].plot(figsize=(10, 5), title='PM10 values')

#Filtre: on utilise les méthodes .rolling() et .median() des dataframes pandas
df_PM10_roll = df_PM10.rolling(win_len).median()
df_PM10_roll.rename(columns={'PM10': 'PM10_median'}, inplace=True)
df_PM10_roll['PM10_median'].plot()


#On affiche le résultat
axPM10.legend(fontsize=8.5)
plt.tight_layout()
plt.show()


# In[45]:


#On ferme le plot pour pouvoir en ouvrir un nouveau dans la prochaine cellule
plt.close()


# In[46]:


#On plot les valeurs brutes, les valeurs médianes et les valeurs filtrées.
#On commence par récupérer les valeurs précédentes.
axPM10B = df_PM10['PM10'].plot(figsize=(10, 5), title='PM10 values')

df_PM10_roll['PM10_median'].plot()

#On récupère l'écart type sur la même fenêtre roulante dans df_PM10_std
df_PM10_std = df_PM10.rolling(win_len).std()

#On filtre: les valeurs s'éloignant trop de la médiane ne sont pas gardées mais on garde les NaN pour une interpolation future
df_PM10_filtered = df_PM10[(df_PM10.PM10 <= df_PM10_roll.PM10_median + 3*df_PM10_std.PM10) & (df_PM10.PM10 >= df_PM10_roll.PM10_median - 3*df_PM10_std.PM10) | pd.isnull(df_PM10.PM10)]
df_PM10_filtered.rename(columns={'PM10': 'PM10_filtered'}, inplace=True)
df_PM10_filtered['PM10_filtered'].plot()

#On récupère les outliers et on les remplace par NaN
df_PM10_outliers = df_PM10[(df_PM10.PM10 > df_PM10_roll.PM10_median + 3*df_PM10_std.PM10) | (df_PM10.PM10 < df_PM10_roll.PM10_median - 3*df_PM10_std.PM10)]
df_PM10_outliers.rename(columns={'PM10': 'PM10_filtered'}, inplace=True)
df_PM10_outliers['PM10_filtered'] = np.nan

#On affiche le résultat
axPM10B.legend(fontsize=8.5)
plt.tight_layout()
plt.show()


# In[47]:


plt.close()


# In[48]:


df_PM10.count()


# In[49]:


df_PM10_final = pd.concat([df_PM10_filtered, df_PM10_outliers])


# In[50]:


df_PM10_final.count()


# In[51]:


df_PM10_final.to_csv(r'PM10_filtered.csv', encoding='utf8', index=False)


# #### Rolling median filter sur PM1.0

# In[52]:


df_PM1 = df[['time','PM1.0']]


# In[53]:


#On plot les valeurs brutes et les valeurs filtrées, c'est à dire la médiane sur une fenêtre mouvante de 10 unités
axPM1 = df_PM1['PM1.0'].plot(figsize=(10, 5), title='PM1.0 values')

#Filtre: on utilise les méthodes .rolling() et .median() des dataframes pandas
df_PM1_roll = df_PM1.rolling(win_len).median()
df_PM1_roll.rename(columns={'PM1.0': 'PM1.0_median'}, inplace=True)
df_PM1_roll['PM1.0_median'].plot()


#On affiche le résultat
axPM1.legend(fontsize=8.5)
plt.tight_layout()
plt.show()


# In[54]:


plt.close()


# In[55]:


#On plot les valeurs brutes, les valeurs médianes et les valeurs filtrées.
#On commence par récupérer les valeurs précédentes.
axPM1B = df_PM1['PM1.0'].plot(figsize=(10, 5), title='PM1.0 values')

df_PM1_roll['PM1.0_median'].plot()

#On récupère l'écart type sur la même fenêtre roulante dans df_PM1_std
df_PM1_std = df_PM1.rolling(win_len).std()

#On filtre: les valeurs s'éloignant trop de la médiane ne sont pas gardées mais on garde les NaN pour une interpolation future
df_PM1_filtered = df_PM1[(df_PM1['PM1.0'] <= df_PM1_roll['PM1.0_median'] + 3*df_PM1_std['PM1.0']) & (df_PM1['PM1.0'] >= df_PM1_roll['PM1.0_median'] - 3*df_PM1_std['PM1.0']) | pd.isnull(df_PM1['PM1.0'])]
df_PM1_filtered.rename(columns={'PM1.0': 'PM1.0_filtered'}, inplace=True)
df_PM1_filtered['PM1.0_filtered'].plot()

#On récupère les outliers et on les remplace par NaN
df_PM1_outliers = df_PM1[(df_PM1['PM1.0'] > df_PM1_roll['PM1.0_median'] + 3*df_PM1_std['PM1.0']) | (df_PM1['PM1.0'] < df_PM1_roll['PM1.0_median'] - 3*df_PM1_std['PM1.0'])]
df_PM1_outliers.rename(columns={'PM1.0': 'PM1.0_filtered'}, inplace=True)
df_PM1_outliers['PM1.0_filtered'] = np.nan

#On affiche le résultat
axPM1B.legend(fontsize=8.5)
plt.tight_layout()
plt.show()


# In[56]:


plt.close()


# In[57]:


df_PM1.count()


# In[58]:


df_PM1_final = pd.concat([df_PM1_filtered, df_PM1_outliers])


# In[59]:


df_PM1_final.count()


# In[60]:


df_PM1_final.to_csv(r'PM1.0_filtered.csv', encoding='utf8', index=False)


# #### Rolling median filter sur PM2.5

# In[61]:


df_PM2 = df[['time','PM2.5']]


# In[62]:


#On plot les valeurs brutes et les valeurs filtrées, c'est à dire la médiane sur une fenêtre mouvante de 10 unités
axPM2 = df_PM2['PM2.5'].plot(figsize=(10, 5), title='PM2.5 values')

#Filtre: on utilise les méthodes .rolling() et .median() des dataframes pandas
df_PM2_roll = df_PM2.rolling(win_len).median()
df_PM2_roll.rename(columns={'PM2.5': 'PM2.5_median'}, inplace=True)
df_PM2_roll['PM2.5_median'].plot()


#On affiche le résultat
axPM2.legend(fontsize=8.5)
plt.tight_layout()
plt.show()


# In[63]:


plt.close()


# In[64]:


#On plot les valeurs brutes, les valeurs médianes et les valeurs filtrées.
#On commence par récupérer les valeurs précédentes.
axPM2B = df_PM2['PM2.5'].plot(figsize=(10, 5), title='PM2.5 values')

df_PM2_roll['PM2.5_median'].plot()

#On récupère l'écart type sur la même fenêtre roulante dans df_PM2_std
df_PM2_std = df_PM2.rolling(win_len).std()

#On filtre: les valeurs s'éloignant trop de la médiane ne sont pas gardée mais on garde les NaN pour une interpolation future
df_PM2_filtered = df_PM2[(df_PM2['PM2.5'] <= df_PM2_roll['PM2.5_median'] + 3*df_PM2_std['PM2.5']) & (df_PM2['PM2.5'] >= df_PM2_roll['PM2.5_median'] - 3*df_PM2_std['PM2.5']) | pd.isnull(df_PM2['PM2.5'])]
df_PM2_filtered.rename(columns={'PM2.5': 'PM2.5_filtered'}, inplace=True)
df_PM2_filtered['PM2.5_filtered'].plot()

#On récupère les outliers et on les remplace par NaN
df_PM2_outliers = df_PM2[(df_PM2['PM2.5'] > df_PM2_roll['PM2.5_median'] + 3*df_PM2_std['PM2.5']) | (df_PM2['PM2.5'] < df_PM2_roll['PM2.5_median'] - 3*df_PM2_std['PM2.5'])]
df_PM2_outliers.rename(columns={'PM2.5': 'PM2.5_filtered'}, inplace=True)
df_PM2_outliers['PM2.5_filtered'] = np.nan

#On affiche le résultat
axPM2B.legend(fontsize=8.5)
plt.tight_layout()
plt.show()


# In[65]:


plt.close()


# In[66]:


df_PM2.count()


# In[67]:


df_PM2_final = pd.concat([df_PM2_filtered, df_PM2_outliers])


# In[68]:


df_PM2_final.count()


# In[69]:


df_PM2_final.to_csv(r'PM2.5_filtered.csv', encoding='utf8', index=False)


# #### Rolling median filter sur BC

# In[70]:


df_BC = df[['time','BC']]


# In[71]:


#On plot les valeurs brutes et les valeurs filtrées, c'est à dire la médiane sur une fenêtre mouvante de 10 unités
axBC = df_BC['BC'].plot(figsize=(10, 5), title='BC values')

#Filtre: on utilise les méthodes .rolling() et .median() des dataframes pandas
df_BC_roll = df_BC.rolling(win_len).median()
df_BC_roll.rename(columns={'BC': 'BC_median'}, inplace=True)
df_BC_roll['BC_median'].plot()


#On affiche le résultat
axBC.legend(fontsize=8.5)
plt.tight_layout()
plt.show()


# In[72]:


plt.close()


# In[73]:


#On plot les valeurs brutes, les valeurs médianes et les valeurs filtrées.
#On commence par récupérer les valeurs précédentes.
axBCB = df_BC['BC'].plot(figsize=(10, 5), title='BC values')

df_BC_roll['BC_median'].plot()

#On récupère l'écart type sur la même fenêtre roulante dans df_PM2_std
df_BC_std = df_BC.rolling(win_len).std()

#On filtre: les valeurs s'éloignant trop de la médiane ne sont pas gardée mais on garde les NaN pour une interpolation future
df_BC_filtered = df_BC[(df_BC.BC <= df_BC_roll.BC_median + 3*df_BC_std.BC) & (df_BC.BC >= df_BC_roll.BC_median - 3*df_BC_std.BC) | pd.isnull(df_BC.BC)]
df_BC_filtered.rename(columns={'BC': 'BC_filtered'}, inplace=True)
df_BC_filtered['BC_filtered'].plot()

#On récupère les outliers et on les remplace par Nan
df_BC_outliers = df_BC[(df_BC.BC > df_BC_roll.BC_median + 3*df_BC_std.BC) | (df_BC.BC < df_BC_roll.BC_median - 3*df_BC_std.BC)]
df_BC_outliers.rename(columns={'BC': 'BC_filtered'}, inplace=True)
df_BC_outliers['BC_filtered'] = np.nan

#On affiche le résultat
axBCB.legend(fontsize=8.5)
plt.tight_layout()
plt.show()


# In[74]:


plt.close()


# In[75]:


df_BC.count()


# In[76]:


df_BC_final = pd.concat([df_BC_filtered, df_BC_outliers])


# In[77]:


df_BC_final.count()


# In[78]:


df_BC_final.to_csv(r'BC_filtered.csv', encoding='utf8', index=False)


# #### Explications

# #Valeurs de NO2
# 
# Il y a beaucoup de mesures de NO2 (52007 mesures non nulles sur 59972 mesures).
# 
# Néanmoins, on constate qu'il y a proportionnellement peu d'outliers: 1587 seulement d'après notre traitement.
# 
# Ainsi, les valeurs de NO2 sont très peu bruitées et semblent plutôt bonnes.

# #Valeurs des différents PMx
# 
# Pour les différentes valeurs de PMx, il y a le même nombre de mesures non nulles: 11429 sur 59972 mesures, ce qui est peu.
# 
# PM2.5 possède le moins d'outliers:  1017 d'après notre méthode.
# 
# PM1.0 et PM10 en possèdent tous les deux un peu plus, avec 1034 outliers chacun.
# 
# Il y a ainsi beaucoup de bruit dans les mesures des différents PMx.

# #Valeurs de BC
# 
# Il y a également peu de mesures de BC non nulles: 17631 sur 59972.
# 
# Néanmoins, il y a moins d'outliers: 482 d'après notre traitement.
# 
# Ainsi, les valeurs de BC sont plus nombreuses que celles des PMx et moins bruitées, mais moins nombreuses et plus bruitées que celles de NO2.

# ### 4) ARIMA model

# #Une série chronologique peut être décomposée en 3 composantes : 
# Tendance: mouvement ascendant et descendant des données avec le temps sur une longue période
# Saisonnalité: variance saisonnière 
# Bruit: pointes et creux à intervalles aléatoires
#     
# Avant d'appliquer un modèle statistique sur une série chronologique, nous voulons nous assurer qu'il est stationnaire :
# 	la moyenne des séries ne devrait pas être en fonction du temps
# 	la variance des séries ne devrait pas être en fonction du temps
# 	Aussi la covariance ne doit pas être en fonction du temps
# 
# Nous allons utiliser la bibliothèque statsmodels fournit une suite de fonctions pour travailler avec des données de séries chronologiques
# 
# Comme mentionné précédemment, avant de pouvoir construire un modèle, nous devons nous assurer que la série chronologique est stationnaire. Il existe deux façons principales de déterminer si une série chronologique donnée est stationnaire, et nous avons choisi d’utiliser la suivante :
# 	Rolling statics : tracez la rolling mean et le rolling écart-type . La série chronologique est stationnaire si elle reste constante dans le temps (à l'œil nu on peut voir si les lignes sont droites et parallèles à l'axe des x).
# 

# In[3]:


df = pd.read_csv("data.csv")


# In[4]:


df = df.rename({'PM1.0':'PM1'}, axis='columns')
df = df.rename({'PM2.5':'PM2'}, axis='columns')


# #### a) PM10:

# In[5]:


df_PM10 = df[['time','PM10']]


# In[6]:


rolling_mean = df_PM10.rolling (window = 12) .mean () 
rolling_std = df_PM10.rolling (window = 12) .std () 
df_PM10['PM10'].plot()
rolling_mean.rename(columns={'PM10': 'PM10_mean'}, inplace=True)
rolling_mean['PM10_mean'].plot()
rolling_std.rename(columns={'PM10': 'PM10_std'}, inplace=True)
rolling_std['PM10_std'].plot()
#plt.plot (df, color = 'blue', label = 'Original') 
#plt.plot ( rolling_mean, color = 'red', label = 'Rolling Mean') 
#plt.plot (rolling_std, color = 'black', label = 'Rolling Std') 
plt.legend (loc = 'best') 
plt.title ('Rolling Écart type moyen et glissant ')
plt.show ()


# #### b) PM2:

# In[7]:


df_PM2 = df[['time','PM2']]


# In[8]:


rolling_mean = df_PM2.rolling (window = 12) .mean () 
rolling_std = df_PM2.rolling (window = 12) .std () 
df_PM2['PM2'].plot()
rolling_mean.rename(columns={'PM2': 'PM2_mean'}, inplace=True)
rolling_mean['PM2_mean'].plot()
rolling_std.rename(columns={'PM2': 'PM2_std'}, inplace=True)
rolling_std['PM2_std'].plot()
#plt.plot (df, color = 'blue', label = 'Original') 
#plt.plot ( rolling_mean, color = 'red', label = 'Rolling Mean') 
#plt.plot (rolling_std, color = 'black', label = 'Rolling Std') 
plt.legend (loc = 'best') 
plt.title ('Rolling Écart type moyen et glissant ')
plt.show ()


# #### c) PM1:

# In[9]:


df_PM1 = df[['time','PM1']]


# In[10]:


rolling_mean = df_PM1.rolling (window = 12) .mean () 
rolling_std = df_PM1.rolling (window = 12) .std () 
df_PM1['PM1'].plot()
rolling_mean.rename(columns={'PM1': 'PM1_mean'}, inplace=True)
rolling_mean['PM1_mean'].plot()
rolling_std.rename(columns={'PM1': 'PM1_std'}, inplace=True)
rolling_std['PM1_std'].plot()
#plt.plot (df, color = 'blue', label = 'Original') 
#plt.plot ( rolling_mean, color = 'red', label = 'Rolling Mean') 
#plt.plot (rolling_std, color = 'black', label = 'Rolling Std') 
plt.legend (loc = 'best') 
plt.title ('Rolling Écart type moyen et glissant ')
plt.show ()


# #### d) NO2:

# In[11]:


df_NO2 = df[['time','NO2']]


# In[12]:


rolling_mean = df_NO2.rolling (window = 12) .mean () 
rolling_std = df_NO2.rolling (window = 12) .std () 
df_NO2['NO2'].plot()
rolling_mean.rename(columns={'NO2': 'NO2_mean'}, inplace=True)
rolling_mean['NO2_mean'].plot()
rolling_std.rename(columns={'NO2': 'NO2_std'}, inplace=True)
rolling_std['NO2_std'].plot()
#plt.plot (df, color = 'blue', label = 'Original') 
#plt.plot ( rolling_mean, color = 'red', label = 'Rolling Mean') 
#plt.plot (rolling_std, color = 'black', label = 'Rolling Std') 
plt.legend (loc = 'best') 
plt.title ('Rolling Écart type moyen et glissant ')
plt.show ()


# #### e) BC:

# In[13]:


df_BC = df[['time','BC']]


# In[14]:


rolling_mean = df_BC.rolling (window = 12) .mean () 
rolling_std = df_BC.rolling (window = 12) .std () 
df_BC['BC'].plot()
rolling_mean.rename(columns={'BC': 'BC_mean'}, inplace=True)
rolling_mean['BC_mean'].plot()
rolling_std.rename(columns={'BC': 'BC_std'}, inplace=True)
rolling_std['BC_std'].plot()
#plt.plot (df, color = 'blue', label = 'Original') 
#plt.plot ( rolling_mean, color = 'red', label = 'Rolling Mean') 
#plt.plot (rolling_std, color = 'black', label = 'Rolling Std') 
plt.legend (loc = 'best') 
plt.title ('Rolling Écart type moyen et glissant ')
plt.show ()


# #Comme vous pouvez le voir pour le PM10, la moyenne mobile et l'écart-type roulant augmentent avec le temps. Par conséquent, nous pouvons conclure que la série chronologique n'est pas stationnaire
# 
# 
# Prendre le logarithme de la variable dépendante pour réduire la vitesse à laquelle la moyenne mobile augmente.

# In[15]:


df_logPM10 = np.log(df_PM10['PM10'])
plt.plot(df_logPM10)


# In[16]:


df_logPM2 = np.log(df_PM2['PM2'])
plt.plot(df_logPM2)


# In[17]:


df_logPM1 = np.log(df_PM1['PM1'])
plt.plot(df_logPM1)


# In[18]:


df_logNO2 = np.log(df_NO2['NO2'])
plt.plot(df_logNO2)


# In[19]:


df_logBC = np.log(df_BC['BC'])
plt.plot(df_logBC)


# #Créons une fonction pour exécuter le test qui détermine si une série temporelle donnée est stationnaire.

# In[20]:


def get_stationarity(timeseries):
    
    # rolling statistics
    rolling_mean = timeseries.rolling(window=12).mean()
    rolling_std = timeseries.rolling(window=12).std()
    
    # rolling statistics plot
    original = plt.plot(timeseries, color='blue', label='Original')
    mean = plt.plot(rolling_mean, color='red', label='Rolling Mean')
    std = plt.plot(rolling_std, color='black', label='Rolling Std')
    plt.legend(loc='best')
    plt.title('Rolling Mean & Standard Deviation')
    plt.show(block=False)


# #Il existe plusieurs transformations que nous pouvons appliquer à une série chronologique pour la rendre stationnaire. Par exemple, nous soustrayons la moyenne mobile.

# In[21]:


rolling_mean = df_logPM10.rolling(window=12).mean()
df_logPM10_minus_mean = df_logPM10 - rolling_mean
df_logPM10_minus_mean.dropna(inplace=True)
get_stationarity(df_logPM10_minus_mean)


# In[22]:


rolling_mean = df_logPM2.rolling(window=12).mean()
df_logPM2_minus_mean = df_logPM2 - rolling_mean
df_logPM2_minus_mean.dropna(inplace=True)
get_stationarity(df_logPM2_minus_mean)


# In[23]:


rolling_mean = df_logPM1.rolling(window=12).mean()
df_logPM1_minus_mean = df_logPM1 - rolling_mean
df_logPM1_minus_mean.dropna(inplace=True)
get_stationarity(df_logPM1_minus_mean)


# In[24]:


rolling_mean = df_logNO2.rolling(window=12).mean()
df_logNO2_minus_mean = df_logNO2 - rolling_mean
df_logNO2_minus_mean.dropna(inplace=True)
get_stationarity(df_logNO2_minus_mean)


# In[25]:


rolling_mean = df_logBC.rolling(window=12).mean()
df_logBC_minus_mean = df_logBC - rolling_mean
df_logBC_minus_mean.dropna(inplace=True)
get_stationarity(df_logBC_minus_mean)


# #Comme nous pouvons le voir, après soustraction de la moyenne, la moyenne mobile et l'écart type sont approximativement horizontaux. Par conséquent, la série chronologique est stationnaire.
# 
# 
# #L'application de la décroissance exponentielle est une autre façon de transformer une série temporelle telle qu'elle est stationnaire.

# In[26]:


rolling_mean_exp_decay = df_logPM10.ewm (halflife = 12, min_periods = 0, adjust = True) .mean () 
df_logPM10_exp_decay = df_logPM10 - rolling_mean_exp_decay 
df_logPM10_exp_decay.dropna (inplace = True) 
get_stationarity(df_logPM10_exp_decay)


# #La décroissance exponentielle a donné de moins bons résultats que la soustraction de la moyenne mobile. Cependant, il est toujours plus stationnaire que l'original.
# 
# 
# #Essayons une autre méthode pour déterminer s'il existe une solution encore meilleure. Lors de l'application du décalage horaire, nous soustrayons chaque point par celui qui l'a précédé.

# In[27]:


df_logPM10_shift = df_logPM10 - df_logPM10.shift()
df_logPM10_shift.dropna(inplace=True)
get_stationarity(df_logPM10_shift)


# #Le décalage temporel a donné de moins bons résultats que la soustraction de la moyenne mobile. Cependant, il est toujours plus stationnaire que l'original.

# In[ ]:


# = seasonal_decompose(df_logPM10) 
#model = ARIMA(df_logPM10, order=(2,1,2))
#results = model.fit(disp=-1)
#plt.plot(df_log_shiftPM10)
#plt.plot(results.fittedvalues, color='red')


# # II- Correction de valeurs nuls par des interpolations

# # III- Modèles de classification sur la colonne activity

# ## Préparation des données (Pour la classification)

# In[28]:



""" Pour plus de flexibilité nous choisissons de transformer nos données en données numériques
    Pour cela on utilise cette fonction avec les hypothèses ci-dessous mentionnées """

# "NULL"= "0"
# Bureau = 1
# Parc = 2
# Domicile = 3
# Magasin = 4
# Métro = 5
# Restaurant = 6
# Rue = 7
# Voiture = 8

# "Arrêter De Cuisiner"= "1"
# "Repos"= "2"
# "Fermeture De Fenêtre"= "3"
# "Fumer"= "4"
# "Ouverture De Fenêtre"= "5"
# "Marcher"= "6"

def fromQualitativeToQuanlitative(df):
            df = df.withColumn('PM2', when(df.PM2=="NULL", "0").otherwise(df.PM2))
            df = df.withColumn('PM10', when(df.PM10=="NULL", "0").otherwise(df.PM10))
            df = df.withColumn('PM1', when(df.PM1=="NULL", "0").otherwise(df.PM1))
            df = df.withColumn('NO2', when(df.NO2=="NULL", "0").otherwise(df.NO2))
            df = df.withColumn('BC', when(df.BC=="NULL", "0").otherwise(df.BC))
        
            df = df.withColumn('activity', when(df.activity=="NULL", "0").otherwise(df.activity))
            df = df.withColumn('activity', when(df.activity=="Bureau", "1").otherwise(df.activity))
            df = df.withColumn('activity', when(df.activity=="Parc", "2").otherwise(df.activity))
            df = df.withColumn('activity', when(df.activity=="Domicile", "3").otherwise(df.activity))
            df = df.withColumn('activity', when(df.activity=="Magasin", "4").otherwise(df.activity))
            df = df.withColumn('activity', when(df.activity=="Métro", "5").otherwise(df.activity))
            df = df.withColumn('activity', when(df.activity=="Restaurant", "6").otherwise(df.activity))
            df = df.withColumn('activity', when(df.activity=="Rue", "7").otherwise(df.activity))
            df = df.withColumn('activity', when(df.activity=="Voiture", "8").otherwise(df.activity))
            
            
            df = df.withColumn('event', when(df.event=="NULL", "0").otherwise(df.event))
            df = df.withColumn('event', when(df.event=="Arrêter De Cuisiner", "1").otherwise(df.event))
            df = df.withColumn('event', when(df.event=="Repos", "2").otherwise(df.event))
            df = df.withColumn('event', when(df.event=="Fermeture De Fenêtre", "3").otherwise(df.event))
            df = df.withColumn('event', when(df.event=="Fumer", "4").otherwise(df.event))
            df = df.withColumn('event', when(df.event=="Ouverture De Fenêtre", "5").otherwise(df.event))
            df = df.withColumn('event', when(df.event=="Marcher", "6").otherwise(df.event))
            
            return df


# In[29]:


df_3 = fromQualitativeToQuanlitative(df_2)

# Assign column names to `columns`
columns = ['PM2', 'PM10', 'PM1', 'NO2', 'BC', 'activity', 'event']

# Conver the `df1` columns to `IntegerType()`
df_3 = convertColumn(df_3, columns, IntegerType())


# In[30]:


# Ensemble de données utiles
df_activity = df_3.filter(df_3.PM2 != 0).filter(df_3.PM10 != 0).filter(df_3.PM1 != 0).filter(df_3.NO2 != 0)
      #.filter(df_3.BC != 0).filter(df_3.event != 0)
    
# Ensemble de données à prédire 
df_activity_To_Predict = df_activity.filter(df_3.activity == 0)

# Ensemble de données à utiliser pour notre modèle
df_activity_data = df_activity.filter(df_3.activity != 0)


# In[31]:


print(df_activity_data.count())
print(df_activity_To_Predict.count())


# In[32]:


from pyspark.ml.feature import VectorAssembler

vectorAssembler = VectorAssembler(inputCols = ['PM1', 'PM10', 'PM2', 'NO2', 'BC', 'event'], outputCol = 'features')
df_activity_data = vectorAssembler.transform(df_activity_data)
df_activity_data = df_activity_data.select(['features', 'activity'])
df_activity_data.show(3)


# In[33]:


"""Division du jeu de données en 2 parties: Une partie pour l'entrainement """
splits = df_activity_data.randomSplit([0.8, 0.2])
train_data = splits[0]
test_data = splits[1]


# ## Random Forest

# In[34]:


from pyspark.ml.classification import RandomForestClassifier
"""Entrainement du modèle:"""
# labelCol c'est la colonne à prédire
# featuresCol regroupe l'ensemble des variables prédictives
# numTrees c'est le nombre maximal d'abres dans la foret, qui vaut 100
# maxDepth C'est la profondeur maximale d'un arbre

rf = RandomForestClassifier(labelCol="activity", featuresCol="features", numTrees = 100, maxDepth = 4, maxBins = 32)
# Train model with Training Data
rfModel = rf.fit(train_data)

""" Using our random forest model to make some predictions: """
predictions = rfModel.transform(test_data)
predictions.select("prediction","activity","features").show()


# In[35]:


#   Evaluator for Multiclass Classification, which expects two input
#   columns: prediction and label.

from pyspark.ml.evaluation import MulticlassClassificationEvaluator
evaluator = MulticlassClassificationEvaluator(predictionCol="prediction")
predictionss= predictions.withColumnRenamed("activity", "label")
print("AUC=")
evaluator.evaluate(predictionss, {evaluator.metricName: "accuracy"})


# In[36]:


# PREDICTION DES NOUVELLES VALEURS
#vectorAssembler = VectorAssembler(inputCols = ['PM1', 'PM10', 'PM2', 'NO2', 'BC', 'event'], outputCol = 'features')
df_activity_predict = vectorAssembler.transform(df_activity_To_Predict)
df_activity_predict = df_activity_predict.select(['features', 'activity'])

predictions_new_value = rfModel.transform(df_activity_predict)


# In[37]:


predictions_new_value.select("prediction","activity","features").show(100)


# # IV- Enrichissement des données

# In[ ]:




